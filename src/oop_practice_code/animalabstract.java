package oop_practice_code;

public abstract class animalabstract {

    public abstract void makeSound();

    public void sleep() {
        System.out.println("Zzz");
    }
}

class dog extends animalabstract {

    // Implementation of the abstract method
    public void makeSound() {
        System.out.println("Woof");
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        animalabstract myDog = new dog();
        myDog.makeSound(); // Outputs "Woof"
        myDog.sleep();
    }
}
