package oop_practice_code;

public class ClassExplanation {
    
    String Company;
    String Model;
    String song;
    String song1;
    
    public void information() {
        System.out.println("i have : " + Company+ "  of model:"  + Model);
    }
    
    public void playsong() {
        System.out.println("i have played this song " + song  );
    }

    
    public class InheritanceExplanation extends ClassExplanation {
    	
    	
    	public void stop() {
            System.out.println("but i dont like this " + song + " so i swithch to other song which is my fevourite one and that is : " + song1);
        }
       public class MultilevelInheritance {

			public String memory;

			public void space() {
				
				 System.out.println("my mobile  " + Company + Model + " has " + memory+ " memory " );
				
			}

        }
    }

  

    public static void main(String[] args) {
        ClassExplanation m1 = new ClassExplanation();
        ClassExplanation.InheritanceExplanation m2 = m1.new InheritanceExplanation();
        ClassExplanation.InheritanceExplanation.MultilevelInheritance m3 = m2.new MultilevelInheritance();
        
        m2.Company = "samsung";
        m2.Model = "m31s";
        m2.song="chan se jo tute koi sapna";
        m2.song1="yahi umer hai karle galti se mistake";
        		
        m2.information();
        m2.playsong();
        m2.stop();
        m3.memory = "128gb";
        m3.space();
    }
}
