package oop_practice_code;

public class Overriding {
    String brandName;
    String model;
    String camera;

    public void information() {
        System.out.println("Company Name: " + brandName + ", Model Name: " + model);
    }

    public void cameraQuality() {
        System.out.println("This " + brandName + " " + model + " has " + camera + " camera.");
    }

    public static class MethodOverriding extends Overriding {
        @Override
        public void cameraQuality() {
            System.out.println("Camera quality is great.");
        }
    }

    public static void main(String[] args) {
        MethodOverriding override = new MethodOverriding();
        override.brandName = "Samsung";
        override.model = "M31";
        override.camera = "60MP";

        override.information();
        override.cameraQuality();
    }
}
