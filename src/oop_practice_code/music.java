package oop_practice_code;
interface MusicPlayer {
    void play();
    void stop();
}

// Concrete classes implementing the interface
class Mp3Player implements MusicPlayer {
    @Override
    public void play() {
        System.out.println("Playing MP3 file");
    }

    @Override
    public void stop() {
        System.out.println("Stopping MP3 player");
    }
}

class CdPlayer implements MusicPlayer {
    @Override
    public void play() {
        System.out.println("Playing CD track");
    }

    @Override
    public void stop() {
        System.out.println("Stopping CD player");
    }
}


public class music {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  // Reference variables of type interface
        MusicPlayer mp3Player = new Mp3Player();
        MusicPlayer cdPlayer = new CdPlayer();
        
        // Using abstraction through the interface references
        mp3Player.play(); // Output: Playing MP3 file
        mp3Player.stop(); // Output: Stopping MP3 player
        
        cdPlayer.play(); // Output: Playing CD track
        cdPlayer.stop(); // Output: Stopping CD player

	}

}
